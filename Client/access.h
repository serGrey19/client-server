#ifndef ACCESS_H
#define ACCESS_H

#include <QDialog>

namespace Ui {
class access;
}

class access : public QDialog
{
    Q_OBJECT

public:
    explicit access(QWidget *parent = nullptr);
    ~access();

private:
    Ui::access *ui;
};

#endif // ACCESS_H
