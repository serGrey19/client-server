#include "myserver.h"
#include <QSqlError>
#include <QSqlDatabase>
#include "QtSql/QSqlDatabase"
#include "QSqlQuery"


myserver::myserver(){}
myserver::~myserver(){}

void myserver::startServer()
{
    if (this->listen(QHostAddress::Any,5555))
    {
        qDebug()<<"Listening";
    }
    else
    {
        qDebug()<<"Not listening";
    }
}

void myserver::incomingConnection(int socketDescriptor)
{
      socket = new QTcpSocket(this);
      socket->setSocketDescriptor(socketDescriptor);

      connect(socket,SIGNAL(readyRead()),this,SLOT(sockReady()));
      connect(socket,SIGNAL(disconnected()),this,SLOT(sockDisc()));

      qDebug()<<socketDescriptor<<" Client connected";

      socket->write("You are connect");
      qDebug()<<"Send client connect status - YES";
}

void myserver::slotServerRead()
{
    while(socket->bytesAvailable()>0)
    {
        QByteArray array = socket->readAll();

        socket->write(array);
    }



}



void myserver::sockReady()
{

    Data = socket->readAll();
    qDebug()<<"Data: "<<Data;
    QFile file;
    file.setFileName("F:/client-server/Client-server/news.txt");
    if (file.open(QIODevice::ReadOnly|QFile::Text))
    {
        QByteArray fromFile = file.readAll();

        socket->write(fromFile);
        socket->waitForBytesWritten(500);
    }

    file.close();
}

void myserver::access()
{
    //Подключаем базу данных
    QSqlDatabase db;
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("C:/sqlite/access.db");
    db.open();

    //Осуществляем запрос
    QSqlQuery query;
    query.exec("SELECT id, login, pass, access FROM access");

    //Выводим значения из запроса
    while (query.next())
    {
    QString id = query.value(0).toString();
    QString login = query.value(1).toString();
    QString pass = query.value(2).toString();
    QString access = query.value(2).toString();
    socket->write(id+" "+login+" "+pass+" "+access+"\n");
    }



}



void myserver::sockDisc()
{
    qDebug()<<"Disconnect";
    socket->deleteLater();
}
