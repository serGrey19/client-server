#ifndef MYSERVER_H
#define MYSERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QFile>
#include <QSqlDatabase>
#include <QSqlQuery>
class myserver: public QTcpServer
{
    Q_OBJECT
public:
    myserver();
    ~myserver();

    QTcpSocket* socket;
    QByteArray Data;
    bool mess;
    QSqlDatabase db;

public slots:
    void startServer();
    void incomingConnection(int socketDescriptor);
    void sockReady();
    void sockDisc();
    void slotServerRead();
    void access();






};

#endif // MYSERVER_H
